import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix , classification_report

(trainX, trainy), (testX, testy) = cifar10.load_data()

serverTrainX = trainX[0 :24999]
serverTrainY = trainy[0 :24999]
clientTrainX = trainX[25000 :49999]
clientTrainY = trainy[25000 :49999]

client1TrainX = clientTrainX[0 :4999]
client2TrainX = clientTrainX[5000 :9999]
client3TrainX = clientTrainX[10000 :14999]
client4TrainX = clientTrainX[15000 :19999]
client5TrainX = clientTrainX[20000 :24999]

client1TrainY = clientTrainY[0 :4999]
client2TrainY = clientTrainY[5000 :9999]
client3TrainY = clientTrainY[10000 :14999]
client4TrainY = clientTrainY[15000 :19999]
client5TrainY = clientTrainY[20000 :24999]

print(client1TrainX.shape, client1TrainY.shape)
print(client2TrainX.shape, client2TrainY.shape)
print(client3TrainX.shape, client3TrainY.shape)
print(client4TrainX.shape, client4TrainY.shape)
print(client5TrainX.shape, client5TrainY.shape)

np.save('serverTrainX', serverTrainX)
np.save('serverTrainY', serverTrainY)

np.save('client1TrainX', client1TrainX)
np.save('client1TrainY', client1TrainY)
np.save('client2TrainX', client2TrainX)
np.save('client2TrainY', client2TrainY)
np.save('client3TrainX', client3TrainX)
np.save('client3TrainY', client3TrainY)
np.save('client4TrainX', client4TrainX)
np.save('client4TrainY', client4TrainY)
np.save('client5TrainX', client5TrainX)
np.save('client5TrainY', client5TrainY)

serverTestX = testX[0 :4999]
serverTestY = testy[0 :4999]
clientTestX = testX[5000 :9999]
clientTestY = testy[5000 :9999]

client1TestX = clientTestX[0 :999]
client2TestX = clientTestX[1000 :1999]
client3TestX = clientTestX[2000 :2999]
client4TestX = clientTestX[3000 :3999]
client5TestX = clientTestX[4000 :4999]

client1TestY = clientTestY[0 :999]
client2TestY = clientTestY[1000 :1999]
client3TestY = clientTestY[2000 :2999]
client4TestY = clientTestY[3000 :3999]
client5TestY = clientTestY[4000 :4999]

print(client1TestX.shape, client1TestY.shape)
print(client2TestX.shape, client2TestY.shape)
print(client3TestX.shape, client3TestY.shape)
print(client4TestX.shape, client4TestY.shape)
print(client5TestX.shape, client5TestY.shape)

np.save('serverTestX', serverTrainX)
np.save('serverTestY', serverTrainY)

np.save('client1TestX', client1TestX)
np.save('client1TestY', client1TestY)
np.save('client2TestX', client2TestX)
np.save('client2TestY', client2TestY)
np.save('client3TestX', client3TestX)
np.save('client3TestY', client3TestY)
np.save('client4TestX', client4TestX)
np.save('client4TestY', client4TestY)
np.save('client5TestX', client5TestX)
np.save('client5TestY', client5TestY)