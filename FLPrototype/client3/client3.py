import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix , classification_report
from os import path


classes = ["airplane","automobile","bird","cat","deer","dog","frog","horse","ship","truck"]
client3TrainX = np.load('G:/Federated Learning/client3/client3TrainX.npy')
client3TrainY = np.load('G:/Federated Learning/client3/client3TrainY.npy')
client3TestX = np.load('G:/Federated Learning/client3/client3TestX.npy')
client3TestY = np.load('G:/Federated Learning/client3/client3TestY.npy')
client3TrainY = client3TrainY.reshape(-1,)
client3TestY = client3TestY.reshape(-1,)
client3TrainX = client3TrainX / 255.0
client3TestX = client3TestX / 255.0

def plot_sample(self, X, y, index):
	plt.figure(figsize = (15,2))
	plt.imshow(X[index])
	plt.xlabel(self.classes[y[index]])
	plt.show()

def train_model(cnn):
	cnn.fit(client3TrainX, client3TrainY, epochs=10)
	cnn.save("G:/Federated Learning/client3/client3model")

def get_local_model(cnn):
	if(path.exists("G:/Federated Learning/client3/client3model")):
		localModel = models.load_model('G:/Federated Learning/client3/client3model')
	else:
		localModel = train_model(cnn)
	#summary = localModel.summary()
	print("client3")
	scores  = localModel.evaluate(client3TestX,client3TestY)
	y_pred = localModel.predict(client3TestX)
	y_classes = [np.argmax(element) for element in y_pred]
	print(scores)
	return localModel
	#print(classes[y_classes[2]])
	#plot_sample(client3TestX, client3TestY,2)
