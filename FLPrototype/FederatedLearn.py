import client1.client1
import client2.client2
import client3.client3
import client4.client4
import client5.client5
import server.Server
import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix , classification_report
from os import path


globalModel = server.Server.get_global_model()
client1Model = client1.client1.get_local_model(globalModel)
client2Model = client2.client2.get_local_model(globalModel)
client3Model = client3.client3.get_local_model(globalModel)
client4Model = client4.client4.get_local_model(globalModel)
client5Model = client5.client5.get_local_model(globalModel)

##  validate client models and defend attacks

## client model selection and aggregation

## server global model update