import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix , classification_report
from os import path


classes = ["airplane","automobile","bird","cat","deer","dog","frog","horse","ship","truck"]
client2TrainX = np.load('G:/Federated Learning/client2/client2TrainX.npy')
client2TrainY = np.load('G:/Federated Learning/client2/client2TrainY.npy')
client2TestX = np.load('G:/Federated Learning/client2/client2TestX.npy')
client2TestY = np.load('G:/Federated Learning/client2/client2TestY.npy')
client2TrainY = client2TrainY.reshape(-1,)
client2TestY = client2TestY.reshape(-1,)
client2TrainX = client2TrainX / 255.0
client2TestX = client2TestX / 255.0

def plot_sample(self, X, y, index):
	plt.figure(figsize = (15,2))
	plt.imshow(X[index])
	plt.xlabel(self.classes[y[index]])
	plt.show()

def train_model(cnn):
	cnn.fit(client2TrainX, client2TrainY, epochs=10)
	cnn.save("G:/Federated Learning/client2/client2model")

def get_local_model(cnn):
	if(path.exists("G:/Federated Learning/client2/client2model")):
		localModel = models.load_model('G:/Federated Learning/client2/client2model')
	else:
		localModel = train_model(cnn)
	#summary = localModel.summary()
	print("client2")
	scores  = localModel.evaluate(client2TestX,client2TestY)
	y_pred = localModel.predict(client2TestX)
	y_classes = [np.argmax(element) for element in y_pred]
	print(scores)
	return localModel
	#print(classes[y_classes[2]])
	#plot_sample(client2TestX, client2TestY,2)
