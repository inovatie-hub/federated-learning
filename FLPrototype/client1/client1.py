import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix , classification_report
from os import path


classes = ["airplane","automobile","bird","cat","deer","dog","frog","horse","ship","truck"]
client1TrainX = np.load('G:/Federated Learning/client1/client1TrainX.npy')
client1TrainY = np.load('G:/Federated Learning/client1/client1TrainY.npy')
client1TestX = np.load('G:/Federated Learning/client1/client1TestX.npy')
client1TestY = np.load('G:/Federated Learning/client1/client1TestY.npy')
client1TrainY = client1TrainY.reshape(-1,)
client1TestY = client1TestY.reshape(-1,)
client1TrainX = client1TrainX / 255.0
client1TestX = client1TestX / 255.0

def plot_sample(self, X, y, index):
	plt.figure(figsize = (15,2))
	plt.imshow(X[index])
	plt.xlabel(self.classes[y[index]])
	plt.show()

def train_model(cnn):
	cnn.fit(client1TrainX, client1TrainY, epochs=10)
	cnn.save("G:/Federated Learning/client1/client1model")

def get_local_model(cnn):
	if(path.exists("G:/Federated Learning/client1/client1model")):
		localModel = models.load_model('G:/Federated Learning/client1/client1model')
	else:
		localModel = train_model(cnn)
	#summary = localModel.summary()
	print("client1")
	scores  = localModel.evaluate(client1TestX,client1TestY)
	y_pred = localModel.predict(client1TestX)
	y_classes = [np.argmax(element) for element in y_pred]
	print(scores)
	return localModel
	#print(classes[y_classes[2]])
	#plot_sample(client1TestX, client1TestY,2)
