import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix , classification_report
from os import path


classes = ["airplane","automobile","bird","cat","deer","dog","frog","horse","ship","truck"]
client5TrainX = np.load('G:/Federated Learning/client5/client5TrainX.npy')
client5TrainY = np.load('G:/Federated Learning/client5/client5TrainY.npy')
client5TestX = np.load('G:/Federated Learning/client5/client5TestX.npy')
client5TestY = np.load('G:/Federated Learning/client5/client5TestY.npy')
client5TrainY = client5TrainY.reshape(-1,)
client5TestY = client5TestY.reshape(-1,)
client5TrainX = client5TrainX / 255.0
client5TestX = client5TestX / 255.0

def plot_sample(self, X, y, index):
	plt.figure(figsize = (15,2))
	plt.imshow(X[index])
	plt.xlabel(self.classes[y[index]])
	plt.show()

def train_model(cnn):
	cnn.fit(client5TrainX, client5TrainY, epochs=10)
	cnn.save("G:/Federated Learning/client5/client5model")

def get_local_model(cnn):
	if(path.exists("G:/Federated Learning/client5/client5model")):
		localModel = models.load_model('G:/Federated Learning/client5/client5model')
	else:
		localModel = train_model(cnn)
	print("client5")
	scores  = localModel.evaluate(client5TestX,client5TestY)
	y_pred = localModel.predict(client5TestX)
	y_classes = [np.argmax(element) for element in y_pred]
	print(scores)
	return localModel
	#print(classes[y_classes[2]])
	#plot_sample(client5TestX, client5TestY,2)
