import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix , classification_report
from os import path

	
classes = ["airplane","automobile","bird","cat","deer","dog","frog","horse","ship","truck"]
serverTrainX = np.load('G:/Federated Learning/server/serverTrainX.npy')
serverTrainY = np.load('G:/Federated Learning/server/serverTrainY.npy')
serverTestX = np.load('G:/Federated Learning/server/serverTestX.npy')
serverTestY = np.load('G:/Federated Learning/server/serverTestY.npy')
serverTrainY = serverTrainY.reshape(-1,)
serverTestY = serverTestY.reshape(-1,)
serverTrainX = serverTrainX / 255.0
serverTestX = serverTestX / 255.0

def plot_sample(X, y, index):
	plt.figure(figsize = (15,2))
	plt.imshow(X[index])
	plt.xlabel(classes[y[index]])
	plt.show()

def init_model():
	cnn = models.Sequential([
		layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu', input_shape=(32, 32, 3)),
		layers.MaxPooling2D((2, 2)),
		
		layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'),
		layers.MaxPooling2D((2, 2)),
		
		layers.Flatten(),
		layers.Dense(64, activation='relu'),
		layers.Dense(10, activation='softmax')
	])

	cnn.compile(optimizer='adam',
				loss='sparse_categorical_crossentropy',
				metrics=['accuracy'])
	cnn.fit(serverTrainX, serverTrainY, epochs=10)
	cnn.save("G:/Federated Learning/server/servermodel")
	return cnn
	
def get_global_model():
	if(path.exists("G:/Federated Learning/server/servermodel")):
		cnn = models.load_model('G:/Federated Learning/server/servermodel')
	else:
		cnn = init_model()
	#summary = cnn.summary()
	print("server")
	scores  = cnn.evaluate(serverTestX,serverTestY)
	y_pred = cnn.predict(serverTestX)
	y_classes = [np.argmax(element) for element in y_pred]
	print(scores)
	return cnn
	#print(classes[y_classes[2]])
	#plot_sample(serverTestX, serverTestY,2)
