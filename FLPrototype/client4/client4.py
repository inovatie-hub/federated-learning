import tensorflow as tf
from tensorflow.keras import datasets, layers, models
from keras.datasets import cifar10
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix , classification_report
from os import path


classes = ["airplane","automobile","bird","cat","deer","dog","frog","horse","ship","truck"]
client4TrainX = np.load('G:/Federated Learning/client4/client4TrainX.npy')
client4TrainY = np.load('G:/Federated Learning/client4/client4TrainY.npy')
client4TestX = np.load('G:/Federated Learning/client4/client4TestX.npy')
client4TestY = np.load('G:/Federated Learning/client4/client4TestY.npy')
client4TrainY = client4TrainY.reshape(-1,)
client4TestY = client4TestY.reshape(-1,)
client4TrainX = client4TrainX / 255.0
client4TestX = client4TestX / 255.0

def plot_sample(self, X, y, index):
	plt.figure(figsize = (15,2))
	plt.imshow(X[index])
	plt.xlabel(self.classes[y[index]])
	plt.show()

def train_model(cnn):
	cnn.fit(client4TrainX, client4TrainY, epochs=10)
	cnn.save("G:/Federated Learning/client4/client4model")

def get_local_model(cnn):
	if(path.exists("G:/Federated Learning/client4/client4model")):
		localModel = models.load_model('G:/Federated Learning/client4/client4model')
	else:
		localModel = train_model(cnn)
	print("client4")
	scores  = localModel.evaluate(client4TestX,client4TestY)
	y_pred = localModel.predict(client4TestX)
	y_classes = [np.argmax(element) for element in y_pred]
	print(scores)
	return localModel
	#print(classes[y_classes[2]])
	#plot_sample(client4TestX, client4TestY,2)
