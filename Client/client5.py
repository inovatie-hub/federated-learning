import pandas as pd
import seaborn as sns
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.metrics import r2_score,mean_absolute_error,mean_squared_error
import numpy as np


class client5:
	train_data = pd.DataFrame()
	
	def get_duration(x):
		x=x.split(' ')
		hours=0
		mins=0
		if len(x)==1:
			x=x[0]
			if x[-1]=='h':
				hours=int(x[:-1])
			else:
				mins=int(x[:-1])
		else:
			hours=int(x[0][:-1])
			mins=int(x[1][:-1])
		return hours,mins

	def preProcesser(self):
		self.train_data = pd.read_excel("Client/client5_data.xlsx")
		category = ['Airline','Source','Destination','Additional_Info']

		#treating date column
		self.train_data["Date_of_Journey"] = self.train_data["Date_of_Journey"].str.split('/')
		self.train_data['Date'] = self.train_data['Date_of_Journey'].str[0]
		self.train_data['Month'] = self.train_data['Date_of_Journey'].str[1]
		self.train_data['Year'] = self.train_data['Date_of_Journey'].str[2]
		self.train_data['Total_Stops'].unique()
		self.train_data['Route'] = self.train_data['Route'].str.split('→')
		self.train_data['City1'] = self.train_data['Route'].str[0]
		self.train_data['City2'] = self.train_data['Route'].str[1]
		self.train_data['City3'] = self.train_data['Route'].str[2]
		self.train_data['City4'] = self.train_data['Route'].str[3]
		self.train_data['City5'] = self.train_data['Route'].str[4]
		self.train_data['City6'] = self.train_data['Route'].str[5]

		#treating dep time column
		self.train_data['Dep_Time'] = self.train_data['Dep_Time'].str.split(':')
		self.train_data['Dep_Time_Hour'] = self.train_data['Dep_Time'].str[0]
		self.train_data['Dep_Time_Min'] = self.train_data['Dep_Time'].str[1]
		
		#treating the arrival column
		self.train_data['Arrival_Time'] = self.train_data['Arrival_Time'].str.split(' ')
		self.train_data['Arrival_date'] = self.train_data['Arrival_Time'].str[1]
		self.train_data['Time_of_arrival'] = self.train_data['Arrival_Time'].str[0]
		self.train_data['Time_of_arrival'] =self.train_data['Time_of_arrival'].str.split(':')
		self.train_data['Arrival_Time_Hour'] = self.train_data['Time_of_arrival'].str[0]
		self.train_data['Arrival_Time_Min'] = self.train_data['Time_of_arrival'].str[1]

		#treating the duration column
		self.train_data['Duration'] = self.train_data['Duration'].str.split(' ')
		self.train_data['Travel_hours'] = self.train_data['Duration'].str[0]
		self.train_data['Travel_hours'] = self.train_data['Travel_hours'].str.split('h')
		self.train_data['Travel_hours'] = self.train_data['Travel_hours'].str[0]
		self.train_data['Travel_hours'] = self.train_data['Travel_hours']
		self.train_data['Travel_mins'] = self.train_data['Duration'].str[1]
		self.train_data['Travel_mins'] = self.train_data['Travel_mins'].str.split('m')
		self.train_data['Travel_mins'] = self.train_data['Travel_mins'].str[0]

		#treating the total stops column
		self.train_data['Total_Stops'].replace('non-stop','0',inplace=True)
		self.train_data['Total_Stops'] = self.train_data['Total_Stops'].str.split(' ')
		self.train_data['Total_Stops'] = self.train_data['Total_Stops'].str[0]
		self.train_data['Additional_Info'].replace('No Info','No info',inplace=True)

		#exploring the additional info column
		self.train_data['Additional_Info'].replace('No Info','No info',inplace=True)

		#replace missing data
		self.train_data.drop(["City4"], axis = 1, inplace = True)
		self.train_data.drop(["City5"], axis = 1, inplace = True)
		self.train_data.drop(["City6"], axis = 1, inplace = True)
		self.train_data['City3'].fillna('None',inplace = True)
		self.train_data['Arrival_date'].fillna(self.train_data['Date'],inplace=True)
		self.train_data['Travel_mins'].fillna(0,inplace=True)
		self.train_data['City1'].fillna('DEL',inplace=True)
		self.train_data['City2'].fillna('COK',inplace=True)
		self.train_data['Total_Stops'].fillna(0,inplace=True)
		#self.train_data.drop(index=9039,inplace=True,axis=0)

		#object to int
		self.train_data['Total_Stops'] = self.train_data['Total_Stops'].astype('int64')
		self.train_data['Date'] = self.train_data['Date'].astype('int64')
		self.train_data['Month'] = self.train_data['Month'].astype('int64')
		self.train_data['Year'] = self.train_data['Year'].astype('int64')
		self.train_data['Dep_Time_Hour'] = self.train_data['Dep_Time_Hour'].astype('int64')
		self.train_data['Dep_Time_Min'] = self.train_data['Dep_Time_Min'].astype('int64')
		self.train_data['Arrival_date'] = self.train_data['Arrival_date'].astype('int64')
		self.train_data['Arrival_Time_Hour'] = self.train_data['Arrival_Time_Hour'].astype('int64')
		self.train_data['Arrival_Time_Min'] = self.train_data['Arrival_Time_Min'].astype('int64')
		self.train_data['Travel_mins'] = self.train_data['Travel_mins'].astype('int64')
		#self.train_data.drop(index=6474,inplace=True,axis=0)
		self.train_data['Travel_hours'] = self.train_data['Travel_hours'].astype('int64')

		#creating list of different types of columns
		categorical = ['Airline','Source','Destination','Additional_Info','City1','City2','City3']
		numerical = ['Total_Stops','Date','Month','Year','Dep_Time_Hour','Dep_Time_Min','Arrival_date','Arrival_Time_Hour','Arrival_Time_Min','Travel_hours','Travel_mins']

		#treat skewness
		self.train_data['Travel_hours'] = np.log(self.train_data['Travel_hours'])

		self.train_data.drop(["Date_of_Journey"], axis = 1, inplace = True)
		self.train_data.drop(["Route"], axis = 1, inplace = True)
		self.train_data.drop(["Dep_Time"], axis = 1, inplace = True)
		self.train_data.drop(["Arrival_Time"], axis = 1, inplace = True)
		self.train_data.drop(["Year"], axis = 1, inplace = True)
		self.train_data.drop(["Dep_Time_Min"], axis = 1, inplace = True)
		self.train_data.drop(["Time_of_arrival"], axis = 1, inplace = True)
		self.train_data.drop(["Arrival_Time_Min"], axis = 1, inplace = True)
		self.train_data.drop(["Travel_mins"], axis = 1, inplace = True)
		self.train_data.drop(["Duration"], axis = 1, inplace = True)

		#enocde categorical data
		le = LabelEncoder()
		for i in self.train_data.columns:
			if self.train_data[i].dtypes == 'object':
				self.train_data[i] = le.fit_transform(self.train_data[i])


	def buildModel(self, model):
		sc = StandardScaler()
		ds_x = self.train_data.drop('Price',axis=1)
		y = self.train_data['Price']
		dataset = sc.fit_transform(ds_x)
		x = pd.DataFrame(dataset,columns=ds_x.columns)
		x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.3, random_state = 42)
		model.fit(x_train,y_train) 
		pred =  model.predict(x_test)
		#print('Client5 R2 Training Score :',r2_score(y_train, model.predict(x_train)))
		#print('Client5 R2 Test Score     :',r2_score(y_test, pred))
		#print('Client5 Training Score :',model.score(x_train, y_train))
		#print('Client5 Test Score     :',model.score(x_test, y_test))
		return model
