import pandas as pd
import seaborn as sns
from xgboost import XGBRegressor
from sklearn.linear_model import LinearRegression
from sklearn.tree import DecisionTreeRegressor
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, StandardScaler
from sklearn.metrics import r2_score,mean_absolute_error,mean_squared_error
import numpy as np
import Client.client1
import Client.client2
import Client.client3
import Client.client4
import Client.client5
import test
import Server.server

#test
test = test.test()
test.preProcesser()
testModel = test.buildModel()
print('test intercept:', testModel.intercept_)
print('test slope:', testModel.coef_)

#server
server = Server.server.server()
server.preProcesser()
model = server.buildModel()
print('Server intercept:', model.intercept_)
print('Server slope:', model.coef_)

#client1
client1 = Client.client1.client1()
client1.preProcesser()
client1Model = client1.buildModel(model)
print('Client1 intercept:', client1Model.intercept_)
print('Client1 slope:', client1Model.coef_)

#client2
client2 = Client.client2.client2()
client2.preProcesser()
client2Model = client2.buildModel(model)
print('Client2 intercept:', client2Model.intercept_)
print('Client2 slope:', client2Model.coef_)

#client3
client3 = Client.client3.client3()
client3.preProcesser()
client3Model = client3.buildModel(model)
print('Client3 intercept:', client3Model.intercept_)
print('Client3 slope:', client3Model.coef_)

#client4
client4 = Client.client4.client4()
client4.preProcesser()
client4Model = client4.buildModel(model)
print('Client4 intercept:', client4Model.intercept_)
print('Client4 slope:', client4Model.coef_)

#client5
client5 = Client.client5.client5()
client5.preProcesser()
client5Model = client5.buildModel(model)
print('Client5 intercept:', client5Model.intercept_)
print('Client5 slope:', client5Model.coef_)

#averaging
avg_intercept = (client1Model.intercept_ + client2Model.intercept_ + client3Model.intercept_ + client4Model.intercept_ + client5Model.intercept_)/5
print('Avg intercept:', avg_intercept)
